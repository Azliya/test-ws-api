let ws = null

function openWs() {
  let url = document.getElementById('url').value
  ws = new WebSocket("ws://"+url)
  
}

function send() {
  let message = document.getElementById('message').value
  ws.send(message)
}